BearMetal Developments - Classify and Flexiflow Example
-------------------------------------------------------

This content pack provides an example usage scenario for the mythical company
*BearMetal Developments*.  It provides an example of how to use the Universal
Classify and Flexiflow capabilities.

To use this example content, you will need to clone the git repo, then modify
the Classifier rules to match your environment.  The classifier rules in this
demo code use MAC Address matching of Machines, to set the provisioned OS.


Goals
=====

To demonstrate one of the many ways that an operator can provide configuration
to the Universal content to be able to custom classify hosts with Zero Touch
Automation capabilities.

In addition, this content pack demonstrates how to inject the "`hello-world`"
task dynamically in to the Universal Discover workflow.  This demonstrates
how an organization might choose to add custom task capabilities to the
flexible Universal workflows.


Prerequisites
=============

This content pack example relies on the following prerequisites capabilities:

  * minimum DRP Endpoint version v4.8

    * use of the `bmdev-classify-mixed` Profile requires DRP Endpoint v4.9

  * Universal content version v4.8

    * use of the `bmdev-classify-mixed` Profile requires Universal content v4.9

  * `dev-library` content v4.8 or newer for the `hello-world` task injection

**Notes:**

Universal content v4.9 relies on the new `blueprints` features, which are only
available in DRP Endpoint v4.9 version or newer.

The `bmdev-classify-mixed` Profile specifies classifications that utilize the
Universal Pipleine for Image Deploy, but this was not added to Universal until
v4.9 content version release.  The specific Workflow that must be present is
`universal-image-deploy`, along with the correct Pipeline map configuration
in the `universal/workflow-chain-map` Param.

All other classification rules should run fine on v4.8 DRP Endpoint and
Universal content v4.8 or newer.

***WARNING:***

The Classifier rule `generic` will Provision ANY AND ALL systems that boot
against this config to Photon Linux 4.  It should ONLY be used in a carefully
controlled demo/test environment.


Universal Customizations
========================

The `Universal` content and workflows provide a structured way of providing
advanced infrastructure automation.  It also includes several standardized ways
to enhance the workflows with insitu customization.  A brief list of these
customizable exteension points include:

  * Classifiers to make smart decisions about your infrastrucutre, and dynamically
    drive the workflow automation
  * Flexible task injection to add "*pre*" and "*post*" workflow customizations
  * Validators to verify and check that either pre conditions have been met,
    or workflow segment changes have successfully completed as desired
  * Callbacks which interact with any RESTful API endpoint to effect external
    serivice integrations

This content example addresses customization of Universal workflows to use
a Classify set of rules to enable Zero Touch Automation, and to dynamically
inject a custom task with the Flexiflow capability.


Quick Usage Information
=======================

To use this content, perform the following steps.

  * clone the Git repo
  * modify any of the `bmdev-select-<NAME>` profiles appropriately
  * bundle and upload the content to your DRP Endpoint
  * add the selector code Profile (`bmdev-demo-selector-code`) to the `global` Profile

    * example CLI: `drpcli profiles addprofile global bmdev-demo-selector-code`

  * add the selector Param (`bmdev/demo-selector`) that controls which demo scenario to run to the `global` Profile
  * select the Demo to run from the provided list of options on the `bmdev/demo-selector` Param

    * example CLI: `drpcli profiles set global param bmdev/demo-selector to mixed`

  * either:

    * reboot the Machine, and immediately delete the Machine object for a fresh
      "discovery" run of the Machine
    * set the Machine to the `universal-decommission`
    * set the Machine to `universal-discover` Workflow (may not be safe)

***WARNING***: Use of the `generic` Demo type forces every single Machine
to classify as a Photon 4 linux OS for Zero Touch Automation; without any modifications
to this content.  This does require the Photon 4 linux OS ISO is uploaded to the
DRP Endpoint.  *IT MAY DESTROY YOUR EXISTING SYSTEMS*.

More specific details are below.


Classify for Zero Touch Automation
----------------------------------

Universal workflows contain a custom Classify stage as a "post" stage after the
workflow has completed it's work.  This allows the Classifier to make updates to
the Machine object or state of the next steps in the Workflow.  These updates
can effect changes like Workflow chaining, enabling the Zero Touch Automation, etc.

Each Classify stage utilizes a flexible naming construct unique to that stage,
allowing a collection of unique Classify configurations to be updated on the DRP
Endpoint.

The primary starting point is the unique custom name for the given Stage that
the cusotmizations are being applied to.  This content pack demonstrates use
of the `universal-discover` Classify stage named `universal-discover-classification`.
Reviewing the object configuration of this Stage shows us the Param named:

  * `universal/discover-classification-list`

This is identified by the Stage object Param configuration for the Classifier
customization Param named `classify/stage-list-parameter`.  This Param defines
the custom list of Stages to inject which will point to the Classifier to use.

The Stage that is referenced by this Param defines the Classify task and the
Classify customization that will be used.


Implementing the Classify Customizations
========================================

The following steps should be followed to implement the customizations for the
Classify pieces.

  * Determine what Stage you need to create with the Classify rules
  * Determine what param you need to use to specify your Classify rules
  * Create profile with the Classificiation custom Param set to define your Classify rules
  * Define your Classify rules with your custom Param name as the key

This content pack customizes the `universal-discover-classification` Stage,
with the use of the custom `bmdev-classify-data` Stage.

Also implemented is the ability to quickly change Classification for the given specified
machines, based on the value of the Param `bmdev/demo-selector`.  This Param is enabled
by adding the Profile `bmdev-demo-selector-code` to the `global` profile.  Once the
Profile has been added; simply change the value of the Param to switch to a new set of
classifiers identified by the Param `bmdev/demo-selector`.

Ultimately, the Param value `bmdev/demo-selector` adds the Profile with the specific
classification and customization rules, based on the Profile name pattern like:

  * bmdev-select-<NAME>

Where `<NAME>` is the set of Classify rules and customizations to use; for example
`bmdev-select-photon4`, which classifies all machines that match the Classifier rules
(basically, MAC Addresses) for Zero Touch Operation to install Photon 4 Linux only.

Summarizing this exercise examples we have:

  * `universal-discover`: the Universal workflow we are customizing
  * `universal-discover-classification`: is the Stage we are customizing to apply the
    Classify rules
  * `bmdev-classify-data` Stage which implements our custom classifier, using the
    product default `classify` Task
  * `classify/stage-list-parameter`: tells us which custom Param will carry our defined
    Classify rules
  * `universal/discover-classification-list`: is the custom param defined by the above
  * `bmdev/classify-data` is our custom defined Param that will carry the Classify rules

The levels of Param indirect referencing do make it a bit cumbersome to follow, but it
allows the system to use a common set of code, that can flexibly inject different rulesets
at different Workflow and Stage execution.

Ultimately - `bmdev/classify-data` carries our Classification customizations that will:

  * Match a machine based on it's MAC address
  * Assign a Hostname to the Machine
  * Add the Universal specific configurations that drives the Zero Touch Automation
  * Add any other custom Param/Profiles operations defined in the Profile

***NOTE***: The MAC Address matching examples in this content are specific to a given
set of Virtual Machines.  Cloning this content, and modifying (at least) the MAC Addresses
will be necessary to obtain custom Machine matching and Zero Touch Automation on a
per-machine basis.


Other Custom Classsify Use
==========================

It should be noted that the Classify rules expressed in this content pack are merely the
tip of the iceberg in terms of potential and capability.  The Classifier can make decisions
on many different dimensions.  In addition, custom Functions can be added to the Classifier
engine to further enhance and customize it.

Please review the Classify documentation for more information, available at:

  * https://docs.rackn.io/en/latest/doc/content-packages/classify.html

In addition to the Classify system, other ways of providing machine classification operations
can be performed.  One notable method is use of the `rack` plugin - which uses injected
JSON data structures to define "racks of systems", along with custom Param/classify
matching information.


Dynamic Task Insertion (Flexiflow)
----------------------------------

The Flexiflow system allows for custom injection of tasks to an existing workflow, at
the appropriate "*pre*" and "*post*" Stage definitions in the standard Universal workflows.

Similar to the Classify patterns, the Flexiflow system uses the same Param indirection
methodology to define the ultimate name of the Param that will carry the task injection
customization list.  The Param path used in this example content is as follows:

  * `universal-discover`: the Universal workflow we are customizing
  * `universal-discover-pre-flexiflow`: is the Stage we are customizing to apply the
    Flexiflow task injection
  * `flexiflow/list-parameter`: tells us which custom Param will carry our defined
    flexiflow list of tasks
  *  `universal/discover-pre-flexiflow`: the custom Param name to use to define our list,
    defined in the above Stage by the `flexiflow/list-parameter` setting
  * `bmdev-flexiflow-customization` is the custom defined Param that will carry the
    list of tasks to inject in to the Workflow

For other cusotmizations, identify where you wish to inject the custom task by reviewing
the Stage configuration to determine what Param to use to add the cusotmization.  Adjust
which Params you use to inject your custom changes.

Ultimately (using the above example), we would want to add a Param named `universal/discover-pre-flexiflow`
that is evaluated on a Machine, during the `universal-discover` Workflow, with a string list
of additional tasks to add.  So, setting this Param like follows:

  * `drpcli machines set Name:mach-01 param universal/discover-pre-flexiflow to '[ "task1", "task2" ]'`

Replace `Name:mach-01` with an appropriate name, or Machine UUID string.  This will
add `task1` and `task2` to run during the `universal-discover` Workflow Stage named
`universal-discover-pre-flexiflow`.

This Param can be set in any place that ultimately ends up on the Machine object during
the Workflow runtime.  Some examples include:

  * add that Param to the global Profile
  * add the Param directly on a Machine
  * add the Param to a Profile, which is either added on the global Profile, or on the Machine
  * have a Classifier rule add the Param or a Profile with the Param in it, directly to the Machine


Using These Customizations
--------------------------

The targeted use case for this content pack is to drive a "demo" system to quickly
change the dynamic classification rules for a group of Virtual machines.  As such,
several "demo" scenarios can be quickly switched based on the `bmdev/demo-selector`
Param value, which is usually added to the Global Profile.

The `bmdev-demo-selector-code` runs during the `universal-discover-pre-flexiflow` Stage
(in the `universal-discover` Workflow), which removes and adds appropriate Profiles
that control the customizations, which include the Classification rules for the
selected Demo.


Get The Content Pack
====================

First git clone this content to a local directory where you can (optionally) make
customizations.


Customize the Classifier
========================

Customize the Classifier as defined in the above Classification customization section.
These are done in the various Profiles with name patterns like:

  * `bmddev-select-<NAME>`


Bundle and Upload
=================

Perform the usual "bundle" and "upload" of the content pack to your DRP Endpoint.


Set the Global Profile
======================

Ensure that the `bmdev-demo-selector-code` Profile has been added to the `global`
profile.

Add the Param `bmdev/demo-selector` to the Global Profile.

Select the appropriate Demo scenarios on the Param.

**Optionally** - the Profiles can be added directly on an existing machine object,
which will prevent other Machines from being impacted by the classification rules.
However, do not use the "delete" machine object approach to testing this as described
below.  Deleting the Machine object pattern will remove the customizations for that
Machine, defeating the Zero Touch Automation path.


Deploy Your Demo Machines
=========================

There are generally a few patterns for testing the custom implementation in this content
pack.

  *  Delete machine object and reboot
  *  Set the Machine to `universal-decommission` to force cleanup and reset of the Machine,
     then set the Machine to `universal-discover`
  *  Set the Machine to `universal-discover`

The delete machine object path replicates how a brand new system that has not yet
been discovered will be handled by the Zero Touch Automation via the Classifier
rules.

If the control Profile (`bmdev-demo-selector-code`) and Param (`bmdev/demo-selector`)
are set on the Machine object (and NOT the `global` profile), then the Machine object
should not be deleted.

Setting the machine to the `universal-discover` will generally "reset" the machine to
the beginning of the workflow, which will then implement the customizations.

***NOTE*** it may NOT be advisable to set the Machine to the workflow, if the Machine
object is "dirty" - i.e. it contains Param definitions that conflict with our Profile
Param settings values.  Since Params on the "bare" Machine will override settings
values from the Global Profile.

