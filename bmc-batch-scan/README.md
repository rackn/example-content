This content bundle provides a Portal/UX trigger for an operator to
initiate an IPMI Batch Scan operation. The goal of this content bundle
is to identify scanned Machines that are not currently under management
by Digital Rebar Platform (DRP) and then create a Machine object to
manage those newly discovered Machines.

In addition, this process provides a mechanism to provide a custom Task
to run to configure each newly discovered Machine in some capacity. It
is expected that the Task will set the Machines Boot Order appropriately
and reboot the system via PXE to allow it to then run the `universal-discover`
inventory and discovery workflow.

The following configuration steps must be completed before the IPMI Batch
Scan operation and Machine processing can begin.

1. The IPMI Plugin Provider must be installed in the Catalog
2. The IPMI Plugin must be configured with the following minimallly required Params:

  - `ipmi/discover/enabled`: true
  - `ipmi/discover/oobm-default-credentials`: See Note Below
  - `ipmi/discover/oobm-user-provided-credentials`: See Note Below

3. The `docker-context` Plugin Provider must be installed in the Catalog
4. Docker or Podman must be installed alongside the DRP Endpoint service
5. The `drpcli-runner` Context Container must be installed
6. a Resource Broker of type `resource-context` must be configure
7. a Cluster must be created with at least 1 Machine instantiate to operate as the Batch worker/runner

NOTE on `...default-credentials` and `...provided-credentials`:

  This is a Secure Param with a JSON data payload.  The `default-credentials`
  format contains a series of Username and Password values to try to
  authenticate to the BMC Redfish API in the order specified.  Once found,
  the successful values will be recorded on the newly created Machine
  object.

  The `provided-credentials` can be set to explicit IP or MAC match
  Username/Password values.  This is often used when the BMC uses uniquely
  generated username and/or password values and does not use a generic
  known default set of values.

Firing the UX Trigger requires 4 input values to define the IPMI Batch
Scan operation:

1. Filter - must specify valid Runner machines that can operate as Batch worker/Runners
2. Process Task - a task that will Process the Scan results
3. IP Address start of scan range
4. IP Addres end of scan range

Note that the Filter must be a valid API fitler specification which will
identify Machines that are in WorkOrder mode.  The default of:

  - cluster/tags=In(runner-drpcli)

Will match any Machines that were created with the Cluster named `runner-drpcli`.

The Process Task specified in the `default` value should be sufficient, but
this field allow overriding the Process job in the field if necessary.

The default Process job will set a Context on the newly created Machine
to `drpcli-runner` and then adds a Flexiflow Stage named `bmc-process-scan-flexiflow`
to the newly created Machine.  This Flexiflow stage configuration Param
is also named `bmc-process-scan-flexiflow`.

Simply add a Param with that name to the Global Profile, with a list of
Tasks to run on the newly created Machines.  Presumably these are tasks
that would reconfigure the Machine BootOrder, set `universal-discover`
Workflow, and then reboot the Machine, causing it to PXE boot and inventory
the new Machine.





