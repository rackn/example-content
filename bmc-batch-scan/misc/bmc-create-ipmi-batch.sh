#!/usr/bin/env bash

###
#  Initiate a BMC/IPMI batch scan process
###

# Task to run for PostWorkOrderTemplate of batch job
PROCESS="bmc-process-ipmi-batch-scan-results"
# target filter for running batch jobs
FILTER="cluster/tags=In(runner-drpcli)"

# IP Start and End range to scan
IP_S="10.10.7.101"
IP_E="10.10.7.103"

EP="$(drpcli info get | jq -r '.id')"

if [[ -z "$EP" || "$EP" == "<null>" ]]
then
  echo "FATAL: Unable to connect to DRP Endpoint to start Batch Jobs"
  exit 1
fi

help() {
echo "IPMI Plugin must be configured with 'ipmi/discover/*' Params:"
echo 'examples:
  ipmi/discover/enabled: true
  ipmi/discover/from-lease: false
  ipmi/discover/oobm-default-credentials: {data}
  ipmi/discover/oobm-user-provided-credentials: {data}
  ipmi/discover/parallel: 10
  ipmi/discover/workflow: hello-world
'
echo ""
} # end help()

if ! drpcli blueprints exists "$PROCESS"
then
  echo "FATAL: '$PROCESS' Blueprint doesn't exist on the system."
  echo "       Install the content bundle with the blueprint."
  exit 1
fi

echo "Starting batch job on DRP Endpoint '$EP'." 
echo ""

drpcli batches create - << EO_BATCH
---
Description: IPMI Batch Scan and BMC Configure
Meta:
  color: olive
  icon: boxes
SetupWorkOrderTemplate:
  Blueprint: ipmi-initiate-batch-scan
  Filter: "$FILTER"
  Params:
    ipmi/discover/scan-wait-for-complete: true
    ipmi/discover/batch-scan-ips:
      - start_ip: $IP_S
        end_ip: $IP_E
WorkOrderTemplate:
  Blueprint: ipmi-start-batch-scan-from-wo
  Filter: "$FILTER"
PostWorkOrderTemplate:
  Params:
    rs-debug-enable: true
  Blueprint: "$PROCESS"
  Filter: "$FILTER"
EO_BATCH



