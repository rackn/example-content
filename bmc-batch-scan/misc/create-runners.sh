#!/usr/bin/env bash

###
#  Create Resource Broker and Clusters for use by the IPMI Batch
#  Scan and Process operations.
###

# exit on error, especially the 'timeout' call in destroy()
set -e

RB="context-broker"
CL="runner-drpcli"
CTX="drpcli-runner"
CNT="4"

create_runners() {          
  drpcli resource_brokers create '{"Name":"'$RB'","Context":"'$CTX'","Meta":{"BaseContext":"'$CTX'"},"Profiles":["resource-context"],"WorkOrderMode":true,"Runnable":true}'
  drpcli clusters create '{"Name":"'$CL'","Context":"'$CTX'","Meta":{"BaseContext":"'$CTX'"},"Profiles":["universal-application-base-cluster"],"Params":{"cluster/count":'$CNT',"broker/name":"'$RB'"},"WorkOrderMode":true,"Runnable":true}'
  drpcli clusters work_order add Name:$CL universal-application-base-cluster
}                           
       
destroy_runners(){          
  drpcli profiles set $CL param cluster/count to 0
  drpcli clusters work_order add Name:$CL universal-application-base-cluster

  # wait for Machines to be cleaned up, max wait 5 mins, otherwise error out
  echo "Waiting for Cluster Machines to be destroyed."
  timeout 300 bash -c 'while :; do CNT=$(drpcli machines count "cluster/tags=In('$CL')"); [[ "$CNT" == "0" ]] && break || sleep 1; done'

  drpcli clusters removeprofile Name:$CL $CL
  drpcli profiles destroy $CL
  drpcli clusters destroy Name:$CL
  drpcli resource_brokers destroy Name:$RB
}

case $1 in
  create)  create_runners   ;;
  destroy) destroy_runners  ;;
  *) 
    echo "Unknown usage '$1', expected either 'create' or 'destroy'"
    echo "make sure variables are set right inside this script first"
    exit 1
    ;;
esac
