#!/usr/bin/env bash

###
#  Modify the iDRAC bootorder settings via Redfish protocol
#  EXPERIMENTAL - this has not been tested
###

USER="{{ .ParamExpand "ipmi/username" }}"
PASS="{{ .ParamExpand "ipmi/password" }}"
ADDR="{{ if (.Param "ipmi/address-v4") }}{{ .ParamExpand "ipmi/address-v4"}}{{else}}{{ .ParamExpand "ipmi/address" }}{{end}}"

curl -k -u $USER:$PASS -X PATCH \
-H "Content-Type: application/json" \
-d '{
  "Boot": {
    "BootSourceOverrideTarget": "Pxe",
    "BootSourceOverrideEnabled": "Once",
    "BootSourceOverrideMode": "UEFI",
    "BootSourceOverrideTarget@Redfish.AllowableValues": [
      "Pxe",
      "Hdd"
    ]
  },
  "BootOrder": [
    "NIC.Slot.3-1-1",
    "Hdd"
  ]
}' \
https://$ADDR/redfish/v1/Systems/System.Embedded.1
