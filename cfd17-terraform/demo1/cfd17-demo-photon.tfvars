# Virtual Machines from Proxmox 'raven' cluster, transition to Photon
# Linux install, then on completion, run the ansible palybooks specified
# in 'cfd17-demo-photon' profile

machine_count        = 2
pool                 = "raven"
timeout              = "20m"
profiles             = [ "cfd17-demo-photon" ]
filters              = [ "Address=Ne()" ]
