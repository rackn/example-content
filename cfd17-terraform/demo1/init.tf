terraform {
  required_version = ">= 1.4.0"
  required_providers {
    drp = {
      version = ">= 2.3.0"
      source  = "registry.terraform.io/rackn/drp"
    }
  }
}
