resource "drp_machine" "mach" {
  count               = var.machine_count
  timeout             = var.timeout
  pool                = var.pool
  allocate_workflow   = var.workflow_allocate
  deallocate_workflow = var.workflow_deallocate
  add_profiles        = var.profiles
  deallocate_profiles = var.profiles_deallocate
  filters             = var.filters
}

# not a great output format
output "machine_ip" {
  value       = drp_machine.mach[*].address
  description = "Machine.Address"
}

output "machine_id" {
  value       = drp_machine.mach[*].id
  description = "Machine.Uuid"
}

output "machine_name" {
  value       = drp_machine.mach[*].name
  description = "Machine.Name"
}

output "machine_status" {
  value       = drp_machine.mach[*].status
  description = "Machine.PoolStatus"
}
