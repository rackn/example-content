# TF vars

variable "drp_endpoint" {
  type        = string
  default     = "https://127.0.0.1:8092"
  description = "The DRP Endpoint to issue API calls to."

}

variable "drp_key" {
  type        = string
  default     = "rocketskates:r0cketsk8ts"
  description = "A 'user:pass' pair with authorizations to issue API calls."
}

variable "drp_token" {
  type        = string
  default     = ""
  description = "A DRP Token with authorizations to issue API calls."
}

variable "machine_count" {
  type        = number
  default     = 1
  description = "Number of machines to allocate from the pool."
}

variable "pool" {
  type        = string
  default     = "default"
  description = "The pool to allocate machines from."
}

variable "workflow_allocate" {
  type        = string
#  default     = "universal-runbook"
  default     = ""
  description = "Workflow to run when machine is allocated."
}

variable "workflow_deallocate" {
  type        = string
#  default     = "hello-world"
  default     = ""
  description = "Workflow to run when machine is release. (must not be the same as workflow_allocate)"
}

variable "timeout" {
  type        = string
  default     = "5m"
  description = "Workflow timeout value.  For bare metal, may need 120m for hardware lifecycle management (bios, firmware, raid)."
}

variable "profiles" {
  type        = list
  default     = []
  description = "List of Profiles to apply to the Machine on allocation."
}

variable "profiles_allocate" {
  type        = list
  default     = []
  description = "List of Profiles to apply to the Machine on allocation."
}

variable "profiles_deallocate" {
  type        = list
  default     = []
  description = "List of Profiles to apply to the Machine on deallocation."
}

variable "filters" {
  type        = list
  default     = []
  description = "List of filters to narrow machine selection list by."
}



