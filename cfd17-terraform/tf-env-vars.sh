###
#  source this file to set environment variables to impact the
#  CLI usage; see:
#  - https://developer.hashicorp.com/terraform/cli/config/environment-variables
###

# global for all CLI options
#export TF_CLI_ARGS="-input=false -auto-approve"
export TF_CLI_ARGS_apply="-auto-approve"
export TF_CLI_ARGS_destroy="-auto-approve"

# explicit to only "apply" sub-option
#export TF_CLI_ARGS_apply="-auto-approve"

# values 'trace' or 'off'
#export TF_LOG=off

# specify a log file to write to, verbosity based on TF_LOG setting
# both TF_LOG and TF_LOG_PATH must be set to get log output
#export TF_LOG_PATH=./terraform.log

# "false" (or "0") behave as if "-input=false" is set
#export TF_INPUT=0

# other values that could be set
#  - TF_DATA_DIR
#  - TF_WORKSPACE
#  - TF_IN_AUTOMATION
#  - TF_REGISTRY_DISCOVERY_RETRY
#  - TF_REGISTRY_CLIENT_TIMEOUT
#  - TF_CLI_CONFIG_FILE
#  - TF_PLUGIN_CACHE_DIR
#  - TF_IGNORE
