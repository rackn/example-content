This example shows how to add a custom Classification set of rules to
the Universal Workflow system.  This rule uses the `universal-discover`
Workflow as the basis of the example.

In the `universal-discover` Workflow there are a set of Classification
rules that run based on the Param `universal-discover-classification-base-data`
defined `default` Schema stanza.  Our goal with this example is to retain those
default rules, without overriding the Param, or duplicating the rules which
may later change in the product.

The process requires creating a new Classification version 2 set of classification
configuration.  Ultimately we need three primary parts added to the system:

  * a Stage with the Version 2 configuration pieces for Classify to run correctly
  * a configuration to add to a Machine that tells the existing `universal-discover-classification` to add a new Classify
  * a Param that defines the actual Classifiers `test` and `actions` rules to run

In addition to the above, we will be pedantic and ensure we also create the
correct Param "type definition" objects so they are correctly defined on the
system.

Last, we will enable all of the Version 2 Classifiers capabilities for defining
the rules, adding custom `test` and `actions`, and a custom disabler Param.

The parts defined in this Content Bundle are defined by the following files:

```
   .
   ├── meta.yaml
   ├── params
   │   ├── classify-universal-discover-example-data.yaml
   │   ├── classify-universal-discover-example-disable.yaml
   │   ├── classify-universal-discover-example-function.yaml
   │   └── classify-universal-discover-example.yaml
   ├── profiles
   │   └── classify-universal-discover-example.yaml
   └── stages
       └── classify-universal-discover-example.yaml
```

To utilize this example as the basis for a production usage, you will need
at a minimum, the following object definitions:

  * the Stage that defines the Version 2 classifier configuraiton
  * the `data` Param that is the Type definition for our `test` and `actions` that will be defined
  * the Param (in a Profile in this example content) that adds your Stage

The additional (`disable`, `function`, and basic Param the classifier uses
in this example) are optional, but the `disable` and `function` Params *should*
be added to ensure full feature capabilities.

To use this example, use a Machine that is currently in the `universal-discover`
Workflow, and perform the following actions:

  1. Set the `profile` named `classify-universal-discover-example` on the Machine
  2. Re-run the `universal-discover` Workflow

Once the Machine completes the Workflow, you should be able to observer the
Machines Params and see the Param `classify-universal-discover-example` set
to the text value of `added by classifier example`.

