# Context Demo Example and Training Content

![Cloudia the Context Demo](images/context_demo.png)

* [Introduction](#introduction)
* [Runner Context](#runner-context)
* [Usage](#usage)
* [Feature in Videos](#feature-in-videos)
* [Operating Context Demo](#operating-context-demo)
* [Which DRP Endpoint Are You Talking To?](#which-drp-endpoint-are-you-talking-to)
* [NOTES](#notes)
  + [Not All Content Types Represented](#not-all-content-types-represented)
  + [Other Content Examples](#other-content-examples)
  + [Bundle Operation Errors](#bundle-operation-errors)
  + [README.md as Documentation Meta Info](#readme-as-documentation-meta-info)
  + [Talk to Us!](#talk-to-us)


## Introduction

For [Digital Rebar Provision](http://rebar.digital), which is maintained by
[RackN](https://rackn.com).

This `context_demo` repo is an example / training content pack designed to help
new users of [Digital Rebar Provision](http://rebar.digital) to understand how
content packs can be authored and maintained in a Git (or other Source Code
Control System) management system.

The directory structure (in the "`content/`" directory) closely relates to the
different parts of composable content that are represented in Content Bundles
(or Packs).  Each directory contains unique content pieces for each of the
types of content.

Directories that don't exist here (for example "`bootenvs`") can be created if
you need to build content of that type.

All Workflow elements in Content are executed as a "`job`" on the Machine side.
This is why you must have a Runner (agent) running.  DRP supports "`plugins`"
which are compiled binaries, that execute on the DRP Endpoint side, on behalf
of the Machine.  This Context Demo content does NOT relate to Plugins, only
content executed on the Machine as Workflow jobs.

## Runner Context

Contexts are high level objects that allow us to express the idea that a task for a machine can be run from multiple places. A Context is specified in a task list with the `context:` keyword, followed by the name of the context. Most commonly, a system administrator will want to execute a task from the machine itself, such as installing packages, writing config files, and starting services. This is referred to as the "machine context" or an "empty context", because the `context:` keyword is used with no value.

This Content Bundle contains one task that is executed from multiple contexts. It will execute the `context-demo-example` task from the `drpcli-runner` context, then it will execute the same task from the machine context. 

```
Tasks:
- "context:drpcli-runner"
- "context-demo-example"
- "context:"
- "context-demo-example"
```

Note that the task execution logs will denote the change in IP address.

## Example Output

Note that the UUID of the machine doesn't change, but the OS Type does.  

This is because only the context is changing where the machine is executing the script.

```bash
# "context:drpcli-runner" - context-demo-example.sh
INFO: ###############################
INFO: #  using ux://machines/2c9e9f73-1232-49a6-ac89-a668bef87f47 
INFO: #  Machine Role: machine
INFO: #  Machine Type: container
INFO: #  Machine Name: context_demo
INFO: #       OS Type: alpine
INFO: #    OS Version: 3.16.1
INFO: #       OS Name: alpine-3.16.1
INFO: ###############################
```

```bash
# "context:" - context-demo-example.sh
INFO: ###############################
INFO: #  using ux://machines/2c9e9f73-1232-49a6-ac89-a668bef87f47 
INFO: #  Machine Role: machine
INFO: #  Machine Type: container
INFO: #  Machine Name: context_demo
INFO: #       OS Type: ubuntu
INFO: #    OS Version: 22.04
INFO: #       OS Name: ubuntu-22.04
INFO: ###############################
```

## Additional Resources

* https://docs.rackn.io/stable/operators/machine/contexts/?h=context
* https://docs.rackn.io/stable/arch/models/runner/context/?h=context

## Usage

To install the `context_demo` content pack, use the following commands:

```shell
git clone https://gitlab.com/rackn/example-content.git
cd example-content/context_demo/content
drpcli contents bundle ../context_demo-v1.yaml
```

To inject/install it to a Digital Rebar Provision endpoint, use:

```shell
drpcli contents create ../context_demo-v1.yaml
```

If you make changes to the local files (updates, edits, fixes, etc), and
want to update the already installed conent pack, rebundle the changes,
then update as follows:

```shell
# edit files as desired
drpcli contents bundle ../context_demo-v2.yaml
drpcli contents update context_demo ../context_demo-v2.yaml
```


## Feature in Videos!

* Creating Content: https://youtu.be/79Y-3IOguZk
* Bundling: https://youtu.be/JUyzFNkLyZU


## Operating Context Demo

You must have a machine that is currently running an Agent (`runner-service`);
the _Sledgehammer_ (discovery) bootenv meets this criteria, or the
`runner-service` has been run on an installed OS.  Verify the agent is running
with `ps -ef | grep 'drpcli processjobs' | grep -v grep"`.

Add the `context_demo-example` profile to the machine; a cloned version of the
profile with your changes to the _Params_, or the bare params directly to
the machine.

Set the machine to the `context_demo` _Workflow_.  The "**i**" (information) column
should change to the set color and icon.  To rerun the workflow, you must
first clear the workflow (remove) the current `context_demo` workflow, then
re-set it on the machine.


## Which DRP Endpoint are you talking to?

Remember that the `drpcli` client tool by default connects to the address
and port specified as follows:

  `https://127.0.0.1:8092`

Make sure you set the Endpoint (and Username/Password if changed from defaults)
with the appropriate options.  Run `drpcli` by itself to get help output.

## Notes

### Not All Content Types Represented

Digital Rebar Provision content has many parts and pieces.  There are several
types of content that are not represented in the Context Demo.  Empty directories
are not allowed in Git repos, so we can't provide blank directories for your
reference.  DRP `bundle` operations can't deal with errant files in a directory
structure (like "`.gitempty`").

For content types not represented here (for example "`bootenvs`"), simply create
an empty directory of that name, to create content of that type.  Use some of
the examples from other content packs (see [Other Content Examples](#other-content-examples)
for more inspiration.

### Other Content Examples

We at [RackN](https://rackn.com) regularly "crib" or "borrow" from existing
content examples we've created already.  You can too!  To check out a much more
advanced use of a Git repo to host multiple content packs, see:

  * https://gitlab.com/rackn/provision-content

Note that the "`content/`" directory contains the actual "**DRP Community Content**",
bundle, while other content packs exist in the subdirectories "`krib`", 
"`sledgehammer-builder`", etc.

### Bundle Operation Errors

The contents bundle operation is very sensitve to errant/unexpected files
in the directory structure.  Anything that is NOT a meta file (eg a file
with `._Something.meta` or a YAML file will be misinterpreted and an
error will occur on the bundle operation, like:

```shell
drp@pixie:./context_demo$ drpcli contents bundle context_demo-v1.yaml
Error: Failed to load: No idea how to decode LICENSE into dr-provision-releases
```

(`.gitignore` will also similarly cause an error).  Make sure your directory
is clean from errant hidden/dot files, things like `LICENSE`, `README.md`, etc.

For this reason, the `context_demo` contents have been moved in to the subdirectory
named `content/`.


### README as Documentation Meta Info

This README is included (by symbolic reference) as `content/._Documentation.meta`
file.  The Documentation meta file can use RST formatted text.

### Talk to Us!

The [RackN Team](https://rackn.com) uses Slack to communicate with our
[Digital Rebar Provision](http://rebar.digital) community. If you haven't already
signed up for our Community Slack, you can do so at:

  * https://rackn.com/support/slack/

We look forward to helping you with your provisioning automation journey towards
a fully self sustained Continuously Delivered Data Center journey.


