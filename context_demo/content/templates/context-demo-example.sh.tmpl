#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

set -e

# This retrieves the sleep time from the context-demo.sleep-time.yml parameter
# https://gitlab.com/rackn/example-content/-/blob/master/context_demo/content/params/context-demo.sleep-time.yml?ref_type=heads
sleep_time={{ .Param "context-demo/sleep-time" }}

# Print some info about where we're running
job_info "###############################"
job_info "#  using ux://machines/${RS_UUID}"
job_info "#  Machine Role: ${RS_MC_ROLE}"
job_info "#  Machine Type: ${RS_MC_TYPE}"
job_info "#  Machine Name: ${RS_MC_NAME}"
job_info "#       OS Type: ${OS_TYPE}"
job_info "#    OS Version: ${OS_VER}"
job_info "#       OS Name: ${OS_NAME}"
job_info "###############################"

# Try print our IP info
ip addr || ifconfig || /sbin/ip addr || /sbin/ifconfig || echo "IPaddress: $(hostname -I)" || true

# Sleep for a set time.
# To see the context spawn and then clean itself up,
# in a parallel terminal connected to the DRP Server, run the command:
# watch -n 1 "podman ps"
job_info "Sleeping for ${sleep_time} seconds."

for (( count=sleep_time; count>=1; count-- )); do
  job_info "Sleeping for ${count} more seconds"
  sleep 1
done

job_success "good day!"
