# Overview

This is an example pipeline for universal application.

It is demonstrating the following concepts:

  * Callback Notifications
  * Callback-based external classification
  * Hardware classification
  * BOM-based validation

Many of these concepts can be found in this docs [page](https://docs.rackn.io/stable/redirect/?ref=rs_operators_pipelines_classification).

## Callback Notifications

The content pack has examples of how to drive starting and ending notifications to an external service.

To make this happen, the system configures the callback plugin to send data to an external service.  The example also shows how to generate custom data for each callback.

When using universal pipelines, the default pipeline elements have callback stages that can be configured to notify external services of progress.  By default, the callbacks will send complete machine objects as a json blog to the service.

In this example, we want to call the sevice with the IP address of the machine starting the discovery process.  We also want the service notified by Machine name that the machine has completed pipeline.

The discovery pipeline element, universal-discover, has a callback when it starts called `universal-discover-start`.  Once the pipeline completes, the final workflow of the pipeline has a callback called `universal-runbook-complete`.  These are built in but unconfigured by default.  To enable them, the callback plugin needs to know what to do about them.

In the callback plugin, the parameter, `callback/callbacks` define the actions for each callback.  For this example, we will assume that the service is not secured and the POST of a JSON blob to a URL will handle the notification.  Documentation can be [here](https://docs.rackn.io/stable/developers/contents/callback/).

The `callback/callbacks` parameter looks like this:

``` yaml
callback/callbacks:
  universal-discover-start:
    Method": POST
    Retries": 3
    Headers":
      Accept: "application/json"
      Content-Type: "application/json"
    Timeout: 60
    Url: "https://my-service.company.com/start"
  universal-runbook-complete:
    Method": POST
    Retries": 3
    Headers":
      Accept: "application/json"
      Content-Type: "application/json"
    Timeout: 60
    Url: "https://my-service.company.com/complete"
```

By default, the data will be a Machine object.  This is not what the service expects.  The data can be customized. The customization is defined in the profile, [universal-discover-config](profiles/universal-discover-config.yaml) that is added to the global profile's profiles list.  The profile defines the parameter `callback/data-override`.  This will allow the data generated to match the expected protocol with the service.

For the `universal-discover-start` callback, the service expects a JSON blob with a single field of `IP_Address` set to the booting IP address.  For the `universal-runbook-complete` callback, the service expects a JSON object with a single field of "HOSTS" set to the Machine's hostname.  The parameter, `callback/data-override`, is a template that is converted into data.

Since this profile is added to the global profile, all machines will have access to those parameters.  The callback uses the Machine's parameters (direct and inherited) for data.  The `callback/data-override` parameter define in the profile attached to the global profile encodes the data format protocol for the external service.

With these two configuration elements in place, the external service will be notified as Machine's transition through their pipelines.

## External Classification by Callback

To figure out which pipeline should be applied to the Machine during discovery, the universal-discover workflow can call out to an external service to get information about the Machine being processed.

In this example, the same service can handle a GET call to return the "profile" and "hostname" of the Machine.

To enable this, we will need to configure the callback plugin.

Add the `ep-set-profile` entry to the `callback/callbacks` parameter.  It looks like this:

``` yaml
callback/callbacks:
  ep-set-profile:
    Method": GET
    Retries": 3
    Headers":
      Accept: "application/json"
      Content-Type: "application/json"
    Timeout: 60
    Url: 'https://my-service.company.com/lookup?ip={{.Machine.Address}}&switch={{.ParamExpand "ep/switch_name"}}'
```

This will generate a GET request to the service with query parameters of the Machine's Address and the parameter [`ep/switch_name`](params/ep.switch_name.yaml).  The service will return a JSON object with the fields, "profile" and "hostname".

The discovery workflow needs a task [ep-external-query-data](tasks/ep-external-query-data.yaml) to generate the callback and parse the results.  The workflow needs to have that task injected so it will be run during the workflow.  This can be done using the universal-discover post flexiflow parameter, `universal/discover-post-flexiflow`.  To make this available to all machines, that parameter must be added to the global profile.   We reuse the  [universal-discover-config](profiles/universal-discover-config.yaml) that is attached to the global profile to update the new parameter.

``` yaml
universal/discover-post-flexiflow:
  - ep-external-query-data
```

This tells the universal-discover workflow to add the `ep-external-query-data` task to the process.

The content pack will contain the pieces to drive the process.  The callback configuration is not in the content pack due to the configuration requirements of authentication and potential regional requirements around the target service.

Content pack components for this part:

* parameter: [ep/switch_name](params/ep.switch_name.yaml)
  * Defines a parameter that is the switch name of the system to pass to external system to get data
* task: [ep-external-query-data](tasks/ep-external-query-data.yaml)
  * The task sets the ep/switch_name from inventory data.
  * The task parses the results (a base64 encoded json object with two fields, profile and hostname)
  * The task sets the `universal/application` parameter to define the provided "profile".
  * The task sets the hostname of the Machine based upon the provided "hostname" field.
* profile: [universal-discover-config](profiles/universal-discover-config.yaml)
  * the profile defines the injection point for the classification task (universal/discover-post-flexiflow)

## Hardware classification

The content pack contains example hardware classification profiles that show how to pull in profiles automatically based upon naming conventions.  More infomation is [here](https://docs.rackn.io/stable/redirect/?ref=rs_operators_pipelines_classification).

In this example, the assumption is that the pipeline is `ep-v1` and the hardware inventory returns a value of `hwtype1`.

The following profiles define configuration for various configuration layers of bare metal configueration, OS installation, and application configuration.

* profile: [universal-application-ep-v1](profiles/universal-application-ep-v1.yaml)
  * This defines the pipeline configuration.  When the system's parameter `universal/application` is set to `ep-v1`.  The default classifier will attach this profile to the machine.
  * This uses profile inheritance to add two pieces.
    * The base OS of centos-8.3.2011 - the profile `universal-application-centos-8.3.2011'.
    * Validations that should be done durint the deployment of this pipeline.  The profile [ep-hw-validations](profiles/ep-hw-validations.yaml).
  * Additionally, application configuration and installation could be added in this profile as well, but none is specified here.
* profile: [universal-hw-hwtype1-ep-v1](profiles/universal-hw-hwtype1-ep-v1.yaml)
  * This profile is added by the default discovery classifier because ot the matches of the hardware inventory data and the univesal/application value.  More infomation is [here](https://docs.rackn.io/stable/redirect/?ref=rs_operators_pipelines_classification).

This two profiles define the hardware configuration, the OS installation, and the application to deploy.  All of this is automatically applied based upon the default classifier from the data provided by the external classifier.

The third profile, [ep-hw-validations](profiles/ep-hw-validations.yaml), is inherited through the pipeline definition.  It contains configuration turn on validations at various points in the pipeline deployment process.  In this case, the validation is added to the into the universal validation point in universal-hardware, `universal/hardware-post-validation` (much like flexiflow).  The task inject and its cooresponding enabling parameter use redfish to query the hardware configuration status to ensure that the hardware thinks it is functionally configured and no errors are found.

## BOM-based validation

In addition to the automatic classification of the pipeline, the default discovery classifier can attach a profile that defines the validation for the bill-of-materials based upon the hardware classification and pipeline choice.

The profile [universal-bom-hwtype1-ep-v1](profiles/universal-bom-hwtype1-ep-v1.yaml) defines a validation inject point, `univesal/discover-post-validatiion`, for the task `validate-parameters`.  The task is in the validation content pack. Additionally, the `validate/parameters` is a map of parameters and values to ensure are set on the Machine.  In this example, the parameters are inventory parameters that set during the discovery workflow.  Values are matched exactly or functionally checked (ranges, greater than, ...).  If errors are found, the process generates a job failure.

The example profile is applied because the hardware inventory value and the pipeline values match the profile name. More information about classification conventions is [here](https://docs.rackn.io/stable/redirect/?ref=rs_operators_pipelines_classification).

The profile [ep-baseline-ep-v1](profiles/universal-baseline-ep-v1.yaml) is a helper profile that is not used in the deployment process.  It is used during the pipeline development process.  The `universal-baseline` pipeline can be used to record and collect the `universal-hw-hwtype1-ep-v1` and `universal-bom-hwtype1-ep-v1`.  By adding the profile to a machine and setting the `universal/application` parameter to `ep-v1`, the machine can be run through the `universal-baseline` workflow.  The result of this workflow will be the creation of the hardware and bom validation profiles.  These can be extracted into a content pack, edited for the desired values, and deployed into the production environment to enable hardware configuration and validation.  Additional document can be found [here](https://docs.rackn.io/stable/redirect/?ref=rs_universal_hardware) about hardware architecture.