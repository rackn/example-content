# Flexiflow Example

## Overview

This content bundle shows how add tasks dynamically in to an Infrastructure
Pipeline on a Machine.  This technique is used to allow an operator to add
custom Task(s) to an existing Workflow in the Pipeline.


## What Does it Do?

This content bundle defines 2 Tasks in an Infrastructure as Code (IaC) Content
Bundle.  The tasks are set to be run during the `universal-runbook` Workflow;
which is typically the last Workflow run when an installation Infrastructure
Pipeline is run to install a system.

All of the various Universal based Workflows support flexible Task injection
either before or after the actual Workflow Stages are run.  This example chooses
to use the `universal-runbook` Workflow Flexiflow injection.

The Tasks will be run during the `universal-runbook-during-flexiflow` Stage.


## Installation

The installation is relatively straightforward.  Simply `git` checkout the code,
bundle it up, and install it.


### Git Checkout

The code is hosted in the RackN gitlab repo named `example-content` as a
subdirectory, so we will have to perform a _sparse checkout_ to get just
the `flexiflow_example` directory.

Perform the following git checkout tasks to get the code:

``` shell
mkdir example-content
cd example-content
git remote add -f origin git@gitlab.com:rackn/example-content.git
git fetch origin
git checkout origin/master -- flexiflow_example
cd flexiflow_example/content
```


### Bundle it Up

Once you have done the git sparse checkout as specified above, use the `drpcli`
command line to create a Content Bundle.

Make sure you are in the `content` directory after the git checkout, and do:

``` shell
# note that you must be in the 'content' directory to bundle up
drpcli contents bundle /tmp/flexiflow-example.yaml Version=v1.0.0
```

You can specify any valid Version string, prefixed with `v`, this is used to
track updates to the Content Bundle in the Catalog.


### Install It To Your DRP Endpoint

Installing the Content Bundle is very straightforward.  Again, use the `drpcli`
tool as follows:

``` shell
drpcli contents upload /tmp/flexiflow-example.yaml
```

Use the same filename as the one used during the "bundle" operation above.  You
may also have to specify the DRP Endpoint, Username, or Password.  For example:

``` shell
drpcli -E https://10.10.10.10:8092 -U rocketskates -P my_password contents upload /tmp/flexiflow-example.yaml
```


## Operating The Custom Tasks on a Machine

To use the custom Tasks on a Machine, you must first configure the Machine
to inject the tasks at the specified Workflow Stage, and you must run that
Workflow on the Machine.


### Configure the Machine to Inject Tasks

To run the custom Tasks on a given Machine, you must add the Param that the
Flexiflow Stage uses to configure which Tasks to inject.  There is a Profile
in the Content Bundle that has the configuration necessary to make example
usage easy.

Simply add the Profile named `flexiflow-example-tasks` to the given Machine.

The Profile sets the following list (array) type Param to add the tasks
as follows:

``` yaml
universal/runbook-during-flexiflow:
- flexiflow-example-task1
- flexiflow-example-task2
```

This tells the system that during the `universal-runbook-during-flexiflow`
Stage to add the two named Tasks; in the order found in the list.


### Run the Custom Tasks

To use this, a Machine must be configured to inject the dynamic Tasks and then
run the `universal-runbook` Workflow.

There are two simple ways to test the dynamic Task injection; do a full install
by specifiying an Infrastructure Pipeline that contains the `universal-runbook`
Workflow at the end; or just run the `universal-runbook` Workflow on an
existing Machine that hast the Runner (agent) in place and operating.  The
installed system must have a BASH shell for these tasks to work.

If the Machine is currently in `universal-runbook`, simply restart the Workflow
to re-run it, which will also now inject the example tasks.

For new Machine builds, set an Infrastructure Pipeline on the Machine that
includes a Workflow chaining rule to end (or use) the `universal-runbook`
Workflow.


## How To Find The Configuration Param

All Universal based Workflows have a _pre_ and _post_ Flexiflow Stage in them
To determine the custom Param name to use to inject Tasks during that given
Stage, simply do the following:

- Find the Stage in the Workflow
- Review the "API" configuration in the Portal
- or use `drpcli stages show <NAME>` (replace <NAME> with Stage name)

You should see something similar to the following (note the Object output is
trimmed for brevity):

``` yaml
# drpcli stages show universal-runbook-during-flexiflow --format=yaml
Name: universal-runbook-during-flexiflow
Params:
flexiflow/list-parameter: universal/runbook-during-flexiflow
```

In the abbreviated output above; the `flexiflow/list-parameter` defines the
final configuraiton Param to use as `universal/runbook-during-flexiflow`.


## A Note About Compose

By default the Flexiflow system uses the Parameter composition process,
otherwise known as a "Composed Param".  This takes the value of the Param
at all possible places and creates one long list with each instance
amended to the list.

For example, the values found in the `global` profile will be added, then
values on a Stage, then Profile based values from a Machine object, and
finally "bare" Params listed on the Machines state object.

Flexiflow supports a "no-compose" option, but as of Oct 2023, this is not
configured in the Universal workflows.  See the Flexiflow documentation
for further details.

You can see the "Compose" behavior in practice by doing the following:

- on the `global` profile, create a Task injection like:
    ``` yaml
    universal/runbook-during-flexiflow:
        - hello-world
    ```
- NOTE that the `dev-library` content bundle must be installed to get `hello-world` Task

Now run the `universal-runbook` Workflow on the Machine.  The final job
logs (see `Activity` panel for details) should run the following Tasks
in the order shown here (from most recent run to oldest):

- flexiflow-example-task1
- flexiflow-example-task2
- hello-world

This is the combined (Composed) list from the `global` profile and the Profile
added on the machine (`flexiflow-example-tasks`).
