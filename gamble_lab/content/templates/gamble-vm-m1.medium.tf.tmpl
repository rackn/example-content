###
#  Gamble Lab Proxmox Virtual machine specification
#
#  m1.medium
#
#  Type:        m      (machine; general)
#  Generation:  1      (first)
#  Attributes:         (none)
#  Sizing:      medium
###

{{ $root := get . "root" }}
    name                      = each.value
    desc                      = "DRP deployed VM on Proxmox node {{ $root.ParamExpand "proxmox/node"}}"
# PXE option enables the network boot feature
    pxe                       = true
# unless your PXE installed system includes the Agent in the installed
# OS, do not use this, especially for PXE boot VMs
    agent                     = 0
    automatic_reboot          = true
    balloon                   = 2048
#    bios                      = "seabios"
    bios                      = "ovmf"
# boot order MUST include network, this is enforced in the Provider
# Optionally, setting a disk first means that PXE will be used first boot
# and future boots will run off the disk
    boot                      = "order=net0;scsi0"
    cores                     = 2
    cpu                       = "host"
    define_connection_info    = true
    force_create              = false
    hotplug                   = "network,disk,usb,cpu"
#    hotplug                   = "network,disk,usb,cpu,memory"
    kvm                       = true
    memory                    = 4096
    numa                      = false
    onboot                    = false
    oncreate                  = true
    os_type                   = "Linux 5.x - 2.6 Kernel"
# see: https://pve.proxmox.com/wiki/Manual:_qm.conf
# search for "ostype:"
#    qemu_os                   = "l26"
    qemu_os                   = "win10"
    scsihw                    = "virtio-scsi-single"
# lsi lsi53c810 megasas pvscsi virtio-scsi-pci virtio-scsi-single
#    scsihw                    = "lsi53c810"
    sockets                   = 1
    tablet                    = true
    target_node               = "{{ $root.ParamExpand "proxmox/node"}}"
    vcpus                     = 0

    disk {
        cache        = "writeback"
        discard      = "on"
        iothread     = 1
        mbps         = 0
        mbps_rd      = 0
        mbps_rd_max  = 0
        mbps_wr      = 0
        mbps_wr_max  = 0
        replicate    = 0
        size         = "65G"
        ssd          = 1
        storage      = "local-lvm"
# scsi, ide, sata, virtio
        type         = "scsi"
    }

    network {
        bridge    = "vmbr0"
        firewall  = false
        link_down = false
        model     = "e1000"
    }

#    tags = "{{ range $i, $p := $root.Machine.Profiles }}{{lower $p}} {{ end }} {{ $root.Param "proxmox/node" }} digitalrebar"
#    tags = "digitalrebar"

