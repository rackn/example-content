# homepage-runbook

This repository contains configuration for deploying [Homepage](https://gethomepage.dev/main/installation/) to a machine.

Ansible is used in a local-only mode to deploy the application, which is a docker-compose stack.

Apply the profile `homepage-runbook` to a machine to deploy the application in the runbook phase.
