## Pre-requisites

- [just](https://github.com/casey/just) for running commands
- [drpcli](https://docs.rackn.io/stable/operators/cli/) for bundling and deplloying to DRP
- set the following environment variables:
    RS_ENDPOINT, replace with your DRP endpoint
    RS_KEY, replace with your DRP user/password

## Install bundled content into the drp endpoint using creds
```bash
export RS_ENDPOINT="https://192.168.1.3:8092"
export RS_KEY="rocketskates:r0cketsk8ts"
just deploy
```
