#!/bin/bash
# Updates all content packs pending merge

set -e

changed="$1" # $(git status -s | grep " M " | awk -F '[: /]' '{ print $4 }')"
echo "Bundle and Update to $RS_ENDPOINT ..."

for b in $changed; do
    case "$b" in
    	"integrations"|"tools") echo "  skipping $b (not content)";;
	*)
	    if [[ ! -f "$b/$b.yaml" ]]; then
	        pushd $b/content
	        echo "  updating $b/$b.yaml"
	        drpcli contents bundle $b.yaml > /dev/null
	        drpcli contents upload $b.yaml > /dev/null
		rm -v $b.yaml
	        popd
        fi
    ;;
	esac
done

for b in $changed; do
    rm -f $b/$b.yaml
done

echo "Done"
