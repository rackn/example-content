# Webserver Cluster Example Content Bundle

This content bundle demonstrates how to build and orchestrate a multi-node application cluster using Digital Rebar Platform (DRP). It serves as an educational example of combining multiple content bundles to create a complete infrastructure solution.

## Overview

The Webserver Cluster Example creates a simple but production-like web infrastructure consisting of:

- An HAProxy load balancer for traffic distribution
- Multiple Apache web servers as backend nodes
- Automated configuration and orchestration

```mermaid
graph TD
    CLIENT[Internet Traffic] -->|HTTP| LB[HAProxy Load Balancer<br/>lb01.cluster.example.local]
    LB --> WEB1[Web Server<br/>apache01.cluster.example.local]
    LB --> WEB2[Web Server<br/>apache02.cluster.example.local]
    LB --> WEB3[Web Server<br/>apache03.cluster.example.local]
    
    subgraph Cluster Orchestration
        DRP[DRP Endpoint<br/>Cluster Runner] -.-> |Configures| LB
        DRP -.-> |Configures| WEB1
        DRP -.-> |Configures| WEB2
        DRP -.-> |Configures| WEB3
    end
    
    style CLIENT fill:lightgray,stroke:#333,stroke-width:2px,color:black
    style LB fill:#9370db,stroke:#333,stroke-width:2px
    style WEB1 fill:#4169e1,stroke:#333,stroke-width:2px
    style WEB2 fill:#4169e1,stroke:#333,stroke-width:2px
    style WEB3 fill:#4169e1,stroke:#333,stroke-width:2px
    style DRP fill:#ffd700,stroke:#333,stroke-width:2px,color:black

    linkStyle 0,1,2,3,4,5,6,7 stroke:#000,stroke-width:2px
```

## Purpose

This example illustrates several key concepts:

1. **Cluster Orchestration**: How to use DRP's cluster functionality to coordinate multiple machines
2. **Role Separation**: Implementing distinct roles (load balancer, web server) within a cluster
3. **Content Integration**: Combining multiple content bundles (HAProxy, Apache) into a unified solution
4. **Infrastructure as Code**: Defining infrastructure patterns that can be replicated and scaled

## How It Works

### Cluster Context
The cluster orchestration runs in a Docker container on the DRP endpoint, providing a controlled environment for cluster-level operations. This separation allows for:

- Infrastructure setup independent of member nodes
- Centralized configuration management
- Safe execution of cluster-wide operations

### Component Structure
1. **Main Orchestration Profile** (`universal-application-webserver-cluster`)
    - Controls cluster lifecycle
    - Defines machine roles and configurations
    - Coordinates post-provisioning tasks

2. **Role-Specific Profiles**
    - `apache-load-balancer`: HAProxy configuration and setup
    - `apache-server`: Apache web server configuration and setup

3. **Post-Provisioning Flow**
- Node renaming for consistent identification
    - Machine configuration validation
    - (Potential extension point for DNS configuration)

### Orchestration Flow
The cluster orchestration process begins when the DRP endpoint's cluster runner container initiates the deployment. Working through the main orchestration profile, it first allocates machines from the specified resource broker. Each machine is then assigned its appropriate role-specific profile and pipeline based on its designated function:

- Load balancer nodes receive the `universal-application-haproxy-server` pipeline, which installs and configures HAProxy to distribute incoming HTTP traffic across the web server nodes
- Web server nodes receive the `universal-application-apache-web-server` pipeline, installing and configuring Apache to serve web content

The workflow begins by preparing the cluster infrastructure: nodes are renamed according to their roles (e.g., `lb01.cluster.example.local`, `apache01.cluster.example.local`) and DNS records are established. After this initial setup, the provisioning process begins, where each node receives its operating system and role-specific software installation. Load balancer nodes are configured with HAProxy to distribute incoming HTTP traffic, while web server nodes receive the Apache installation and configuration. This creates a fully functional web cluster where incoming traffic is efficiently distributed across multiple web servers, all managed from the centralized DRP endpoint.

## Prerequisites

- DRP v4.14.0 or later
- drp-community-content bundle
- universal content bundle v4.14.0 or later
- haproxy content bundle v1.0.0 or later
- Sufficient resources for:
    - 1+ load balancer nodes
    - 3+ web server nodes (recommended)
    - Network connectivity between all nodes

## Usage

1. **Install Content Bundle**
   ```bash
   cd example-content/webserver-cluster/content
   drpcli contents bundle webserver-cluster.yaml
   drpcli contents upload webserver-cluster.yaml
   ```

2. **Create Cluster**
   ```bash
   drpcli clusters create - <<EOF
   ---
   Name: webserver-demo
   Profiles:
      - universal-application-webserver-cluster
   Workflow: universal-start
   Params:
      broker/name: pool-broker
      webserver-cluster/domain: example.local
   EOF
   ```

## Best Practices Demonstrated

1. **Role Separation**: Clear separation between load balancer and web server roles
   This bundle demonstrates clean separation of concerns through distinct profiles and pipelines for each role. The `webserver-cluster-load-balancer` and `webserver-cluster-apache-server` profiles ensure independent scaling and maintenance of each tier.

2. **Parameterization**: Configurable components for flexibility
   Key settings like cluster domain, node names, and role-specific configurations are exposed as parameters, making the bundle adaptable to different environments without modifying the underlying implementation.

3. **Post-Provisioning Workflow**: Using flexiflow for additional setup tasks
   The bundle leverages DRP's flexiflow system to orchestrate complex post-deployment tasks such as DNS configuration and node renaming, ensuring reliable, repeatable deployments.

4. **Infrastructure Patterns**: Reusable cluster configuration
   The architecture provides a template for building scalable web applications, with separate load balancer and web server tiers managed centrally through DRP.

## Production Extensions

While this is an example bundle, it's designed to be extended for production use. Common additions should include:

1. **Security Enhancements**
    - SSL/TLS implementation
    - Certificate management
    - Security monitoring

2. **Operational Reliability**
    - Health checks implementation
    - Service discovery integration
    - High availability configuration

3. **Monitoring and Maintenance**
    - Monitoring system integration
    - Logging configuration
    - Backup management

## Contributing

This content bundle is part of the RackN example content. Contributions and improvements are welcome through merge requests.

## License

Apache 2.0 License

## Support

Community support is available through standard RackN community channels.
