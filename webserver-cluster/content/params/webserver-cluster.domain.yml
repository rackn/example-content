---
Name: webserver-cluster/domain
Description: Base domain name for the Webserver Cluster infrastructure
Documentation: |
  # Webserver Cluster Domain

  This parameter defines the base domain name used for all cluster-related DNS records.
  It sets the domain suffix for both load balancer and web server nodes in the
  Webserver cluster.

  - Must be a valid domain name
  - DNS infrastructure must be configured to handle the domain
  - Must be unique if running multiple clusters in the same network

  ## Usage
  The cluster domain is used to construct FQDNs for cluster nodes:
  
  ```bash
  # Default domain: example.local
  
  # Results in FQDNs like:
  lb01.clustername.example.local     # Load balancer
  web01.clustername.example.local # Web server
  web02.clustername.example.local # Web server
  web03.clustername.example.local # Web server
  ```

  ## Notes
  
  - The default uses example.local per RFC 2606
  - Change to your actual domain in production
  - Ensures consistent naming across cluster

Meta:
  color: blue      # Blue for networking/DNS related
  icon: globe      # Globe icon represents domains/DNS
  title: Webserver Cluster Base Domain

Schema:
  type: string
  default: example.local

