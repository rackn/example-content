---
Name: webserver-cluster-load-balancer
Description: "Role profile for HAProxy load balancer nodes in Webserver cluster"
Documentation: |
  # HAProxy Load Balancer Role Profile

  This profile defines the configuration and role-specific parameters for
  HAProxy load balancer nodes within a Webserver cluster. It is applied to
  machines designated as load balancers by the cluster orchestration profile.

  ## Role Purpose

  - Distribute HTTP traffic to Apache backend servers
  - Provide load balancing for cluster services
  - Enable high availability for web services

  ## Configuration
  The profile sets essential HAProxy parameters:
  ```yaml
  Params:
    haproxy/base-install-os: photon-5-dde71ec57-install
    haproxy/role: load-balancer
    haproxy/filters:
      - Profiles=Eq(universal-application-apache-web-server) Params.haproxy/role=Eq(backend)
  ```

  ## Integration Points

  - Used by universal-application-webserver-cluster profile
  - Integrated with universal-application-haproxy-server pipeline
  - Coordinates with webeserver-cluster-apache-server profile nodes

  ## Requirements

  - Requires PhotonOS 5.0 or later
  - Network access to backend servers
  - Proper backend filter configuration

  ## Usage Notes
  
  - Applied automatically by cluster orchestration
  - Configures node for load balancer role
  - Filters define backend server selection
  - Supports work order completion tracking

Meta:
  application: cluster
  platform: haproxy
  icon: share alternate       # Represents load balancing
  color: purple      # Standard for network services

Params:
  haproxy/base-install-os: photon-5-dde71ec57-install
  on-complete-work-order-mode: true
  haproxy/filters:
    - Profiles=Eq(universal-application-apache-web-server) Params.haproxy/role=Eq(backend) Profiles=Eq({{ .ParamExpand "cluster/name" }})
  haproxy/role: load-balancer