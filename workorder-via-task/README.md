# Workorder via Task Example Content

## Introduction

A workorder is a specific, atomic task or set of tasks that are designed to be executed on a host or set of hosts. In the context of the Digital Rebar Platform (DRP), a machine will typically run either a Workflow or a Workorder.

This example Content Bundle demonstrates how to trigger a Workorder from a task. The location of the execution of the Workorder is determined by the "Filter:" parameter, which is outlined below.

## Prerequisites

This content bundle assumes that the desired target execution environment of the blueprint is different from the machine. Therefore, we need to create a cluster of runners, which will form our resource pool. This pool should be named "workorder-via-task-runner-pool" (or change the filter accordingly).

To create a cluster of runners, follow these steps:

* Navigate to the Clusters Wizard in DRP.
* Select "New Cluster".
* Name the cluster "workorder-via-task-runner-pool".
* Set the "broker/name" to "context-broker".
* Click "Save" to launch the cluster.

## Usage

To use this content bundle, add the `workorder-via-task-launcher` profile to a machine, then run the `universal-runbook` pipeline.

The profile injects a task into FlexiFlow. This task then schedules the workorder, which executes on a node determined by the "workorder-via-task/filter" parameter.

## Understanding FlexiFlow

FlexiFlow is a powerful feature of DRP that allows for dynamic and flexible workflows. For further documentation on how FlexiFlow works, refer to the [FlexiFlow example](https://gitlab.com/rackn/example-content/-/tree/master/flexiflow_example).

## Conclusion

This content bundle provides a practical example of how to trigger a Workorder from a task in DRP.
